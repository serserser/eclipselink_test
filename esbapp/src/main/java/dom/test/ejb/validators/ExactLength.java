package dom.test.ejb.validators;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;

@Target({ FIELD, METHOD, PARAMETER, ANNOTATION_TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = ExactLength.LengthValidator.class)
@Documented
public @interface ExactLength {

	String message() default "{dom.test.ejb.validators.ExactLength.message}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

	int value();

	@Target({ FIELD, METHOD, PARAMETER, ANNOTATION_TYPE })
	@Retention(RUNTIME)
	@Documented
	@interface List {
		ExactLength[] value();
	}

	class LengthValidator implements ConstraintValidator<ExactLength, String> {

		private int expectedLength;

		@Override
		public void initialize(final ExactLength length) {
			this.expectedLength = length.value();
		}

		@Override
		public boolean isValid(final String value, final ConstraintValidatorContext constraintValidatorContext) {
			boolean isValid = value == null || value.length() == expectedLength;

			if ( !isValid ) {
				constraintValidatorContext.disableDefaultConstraintViolation();
				constraintValidatorContext
						.buildConstraintViolationWithTemplate("{dom.test.ejb.validators.ExactLength.message}")
						.addConstraintViolation();
			}
			return isValid;
		}
	}
}
