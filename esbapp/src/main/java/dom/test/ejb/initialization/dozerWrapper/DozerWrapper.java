package dom.test.ejb.initialization.dozerWrapper;

import javax.ejb.Local;

@Local
public interface DozerWrapper {

	<T> T map(Object source, Class<T> destination);
}
