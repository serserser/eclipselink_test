package dom.test.ejb.initialization.dozerWrapper.impl;

import dom.test.ejb.initialization.dozerWrapper.DozerWrapper;
import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import java.util.Arrays;

@Startup
@Singleton
public class DozerWrapperImpl implements DozerWrapper {

	private static final Logger logger = LoggerFactory.getLogger(DozerWrapperImpl.class);

	private DozerBeanMapper mapper;

	private static final String [] MAPPING_FILES = {
			"mappings/CreateCountryData.xml"
	};

	@PostConstruct
	void initializeMappings() {
		logger.info("---------------- Initializing Dozer mapper");
		mapper = new DozerBeanMapper();
		mapper.setMappingFiles(Arrays.asList(MAPPING_FILES));
	}

	public <T> T map(Object source, Class<T> destination) {
		return mapper.map(source, destination);
	}

}
