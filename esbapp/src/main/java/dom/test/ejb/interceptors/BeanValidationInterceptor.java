package dom.test.ejb.interceptors;

import dom.test.exceptions.validation.InvalidDataException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Arrays;
import java.util.Set;

public abstract class BeanValidationInterceptor<T> {

	private static final Logger logger = LoggerFactory.getLogger(BeanValidationInterceptor.class);

	private BeanValidationInterceptor() {
	}

	private Class<T> validatedClass;

	public BeanValidationInterceptor(Class<T> validatedClass) {
		this.validatedClass = validatedClass;
	}

	@AroundInvoke
	public Object intercept(InvocationContext context) throws Exception {
		T validatedObject = getValidatedObject(context);

		validateBean(validatedObject);

		validateCustomConstraint(validatedObject);

		return context.proceed();
	}

	protected void validateCustomConstraint(T validatedObject) {
		logger.info("No custom validations performed");
	}

	private void validateBean(T item) throws InvalidDataException {
		ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		Validator validator = validatorFactory.getValidator();
		Set<ConstraintViolation<T>> validationResult = validator.validate(item);
		if ( !validationResult.isEmpty() ) {
			StringBuilder builder = new StringBuilder();
			validationResult
					.forEach(violation
							-> builder.append(violation.getPropertyPath())
							.append(": ")
							.append(violation.getMessage())
							.append("; "));
			builder.delete(builder.lastIndexOf(";"), builder.length() - 1);
			throw new InvalidDataException(builder.toString());
		}
	}

	protected T getValidatedObject(InvocationContext context) {
		Object[] parameters = context.getParameters();
		String errorMessage = "No item of class: " + validatedClass.getSimpleName() + " in request";
		return (T) Arrays.stream(parameters)
				.filter(o -> o.getClass().isAssignableFrom(validatedClass))
				.findAny()
				.orElseThrow(() -> new IllegalStateException(errorMessage));
	}
}
