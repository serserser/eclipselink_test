package dom.test.ejb.interceptors;


import dom.test.ejb.data.countryService.CreateCountryData;
import dom.test.ejb.interceptors.bindings.Validated;

import javax.interceptor.Interceptor;

@Interceptor
@Validated
public class CountryValidationInterceptor extends BeanValidationInterceptor<CreateCountryData> {

	public CountryValidationInterceptor() {
		super(CreateCountryData.class);
	}
}
