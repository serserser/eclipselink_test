package dom.test.ejb.interceptors.response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

@Interceptor
public class CreateCountryResponseInterceptor {

	private static final Logger logger = LoggerFactory.getLogger(CreateCountryResponseInterceptor.class);

	@AroundInvoke
	public Object intercept(InvocationContext context) {
		try {
			return context.proceed();
		} catch ( Exception exc ) {
			logger.error("Failed to process request due to: ", exc);
			return "failure: " + exc.getMessage();
		}
	}
}
