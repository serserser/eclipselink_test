package dom.test.ejb.interceptors;

import dom.test.ejb.interceptors.bindings.Logged;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import java.lang.reflect.Method;

@Interceptor
@Logged
public class LoggingInterceptor {

	private static final Logger logger = LoggerFactory.getLogger(LoggingInterceptor.class);

	@AroundInvoke
	public Object intercept(InvocationContext context) throws Exception {
		Method method = context.getMethod();
		String className = method.getDeclaringClass().getName();
		String methodName = method.getName();
		String methodIdentifier = className + "#" + methodName;
		logger.info("Method " + methodIdentifier + " is being called");
		Object result = context.proceed();
		logger.info("Method " + methodIdentifier + " returned");
		return result;
	}
}
