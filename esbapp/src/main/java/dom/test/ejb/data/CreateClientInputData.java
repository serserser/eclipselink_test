package dom.test.ejb.data;

import dom.test.domain.ClientEntityBean;

public class CreateClientInputData {
	private String firstName;
	private String lastName;
	private String countryCode;



	public ClientEntityBean toClientEntityBean() {
		ClientEntityBean entity = new ClientEntityBean();
		entity.setFirstName(this.getFirstName());
		entity.setLastName(this.getLastName());
		entity.setCountryCode(this.getCountryCode());
		return entity;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
}
