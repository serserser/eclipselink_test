package dom.test.ejb.data.countryService;


import dom.test.ejb.validators.ExactLength;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class CreateCountryData {

	@NotNull
	@ExactLength(2)
	@XmlElement(required = true)
	private String code;

	@NotNull
	@Size(max = 100)
	@XmlElement(required = true)
	private String name;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
