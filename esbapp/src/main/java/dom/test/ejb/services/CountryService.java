package dom.test.ejb.services;

import dom.test.domain.CountryEntity;
import dom.test.ejb.data.countryService.CreateCountryData;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;
import java.util.List;


@WebService
public interface CountryService {

	@WebMethod
	List<CountryEntity> getCountries();

	@WebMethod
	CountryEntity getCountryById(
			@WebParam(name = "countryId")
			@XmlElement(required = true)
					Long id);

	@WebMethod
	String createCountry(
			@WebParam(name = "country")
			@XmlElement(required = true)
					CreateCountryData countryEntityBean);
}
