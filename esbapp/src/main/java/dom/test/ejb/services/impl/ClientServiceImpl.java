package dom.test.ejb.services.impl;

import dom.test.dao.ClientDAO;
import dom.test.domain.ClientEntityBean;
import dom.test.ejb.ClientManagerLocal;
import dom.test.ejb.services.ClientService;
import dom.test.ejb.data.CreateClientInputData;
import dom.test.exceptions.validation.InvalidDataException;
import dom.test.exceptions.validation.MissingDataException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jws.WebService;
import java.util.List;

@Stateless
@WebService(name = "CountryService")
public class ClientServiceImpl implements ClientService {

	private static final Logger logger = LoggerFactory.getLogger(ClientServiceImpl.class);

	@EJB
	private ClientManagerLocal clientManagerLocal;

	@Inject
	private ClientDAO clientDAO;

	@Override
	public String createClient(CreateClientInputData inputData) {
		ClientEntityBean entity = inputData.toClientEntityBean();

		try {
			clientManagerLocal.createClient(entity);
		} catch ( InvalidDataException exc ) {
			logger.warn("The provided data was invalid: " + exc.getMessage());
			return "failure: " + exc.getMessage();
		} catch ( MissingDataException exc ) {
			logger.warn("Some data was missing: " + exc.getMessage());
			return "failure: " + exc.getMessage();
		}
		return "success";
	}

	@Override
	public List<ClientEntityBean> getClientsByCountry(String countryCode) {
		return clientDAO.getClientsByCountry(countryCode);
	}
}
