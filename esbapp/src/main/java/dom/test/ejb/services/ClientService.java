package dom.test.ejb.services;

import dom.test.domain.ClientEntityBean;
import dom.test.ejb.data.CreateClientInputData;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ClientService {
	@WebMethod
	String createClient(CreateClientInputData clientEntityBean);

	@WebMethod
	List<ClientEntityBean> getClientsByCountry(String countryCode);
}
