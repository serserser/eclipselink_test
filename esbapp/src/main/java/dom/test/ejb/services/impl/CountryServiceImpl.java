package dom.test.ejb.services.impl;

import dom.test.cache.DictionaryCache;
import dom.test.domain.CountryEntity;
import dom.test.ejb.interceptors.CountryValidationInterceptor;
import dom.test.ejb.interceptors.LoggingInterceptor;
import dom.test.ejb.interceptors.response.CreateCountryResponseInterceptor;
import dom.test.ejb.services.CountryService;
import dom.test.ejb.data.countryService.CreateCountryData;
import dom.test.ejb.initialization.dozerWrapper.DozerWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.jws.WebService;
import java.util.List;

@Stateless
@WebService(name = "CountryService")
@Interceptors({ LoggingInterceptor.class })
public class CountryServiceImpl implements CountryService {

	private static final Logger logger = LoggerFactory.getLogger(CountryServiceImpl.class);

	@PostConstruct
	protected void initialize() {
		logger.info(CountryService.class.getName() + " bean created");
	}

	@PreDestroy
	protected void close() {
		logger.info(CountryService.class.getName() + " bean destroyed");
	}

	@EJB
	private DictionaryCache dictionaryCache;

	@Inject
	DozerWrapper mapper;

	@Override
	public List<CountryEntity> getCountries() {
		return dictionaryCache.getCountries();
	}

	@Override
	public CountryEntity getCountryById(Long id) {
		return dictionaryCache.getCountry(id).orElse(null);
	}


	@Override
	@Interceptors({ CreateCountryResponseInterceptor.class, CountryValidationInterceptor.class })
	public String createCountry(CreateCountryData createCountryData) {
		CountryEntity countryEntityBean = mapper.map(createCountryData, CountryEntity.class);
		dictionaryCache.createCountry(countryEntityBean);
		return "success";
	}
}
