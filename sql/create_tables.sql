-- drop old schema
drop table lbt_clients;
drop table lbt_countries;
drop sequence lbs_ctr_id;
drop sequence lbs_clt_id;

-- create new scheme
create table lbt_countries (
  ctr_id number(10),
  ctr_code nvarchar2(3) not null,
  ctr_name nvarchar2(100),
  
  constraint pk_ctr_id 
    primary key (ctr_id),
  constraint uq_ctr_code
    unique (ctr_code)    
);

create table lbt_clients (
  clt_id number(10),
  clt_first_name nvarchar2(100),
  clt_last_name nvarchar2(100),
  clt_ctr_id number(10),
  
  constraint pk_clt_id 
    primary key (clt_id),
  constraint fk_clt_ctr_id
    foreign key (clt_ctr_id)
    references lbt_countries (ctr_id)
);

-- create sequences
create sequence lbs_clt_id
  start with 0
  minvalue 0
  increment by 1
  nomaxvalue
  nocache
  order;
  
create sequence lbs_ctr_id
  start with 0
  minvalue 0
  increment by 1
  nomaxvalue
  nocache
  order;

insert into lbt_countries values (lbs_ctr_id.nextval, 'USA', 'United States of America');
insert into lbt_countries values (lbs_ctr_id.nextval, 'POL', 'Poland');
insert into lbt_countries values (lbs_ctr_id.nextval, 'RUS', 'Russia');
insert into lbt_countries values (lbs_ctr_id.nextval, 'DEU', 'Germany');
insert into lbt_countries values (lbs_ctr_id.nextval, 'GBR', 'Unitek Kingdom');
insert into lbt_countries values (lbs_ctr_id.nextval, 'UKR', 'Ukraine');
insert into lbt_countries values (lbs_ctr_id.nextval, 'TUR', 'Turkey');

insert into lbt_clients values (lbs_clt_id.nextval, 'janusz', 'testowy', 1);
insert into lbt_clients values (lbs_clt_id.nextval, 'ziutek', 'kowalski', 2);
insert into lbt_clients values (lbs_clt_id.nextval, 'jarosław', 'kaczyński', 2);
insert into lbt_clients values (lbs_clt_id.nextval, 'donald', 'tusk', 3);
insert into lbt_clients values (lbs_clt_id.nextval, 'boris', 'johnson', 5);
insert into lbt_clients values (lbs_clt_id.nextval, 'dimitrij', 'testowyj', 6);
insert into lbt_clients values (lbs_clt_id.nextval, 'ahmed', 'ahmedinejad', 7);
insert into lbt_clients values (lbs_clt_id.nextval, 'mahmud', 'mahmedimejad', 7);
insert into lbt_clients values (lbs_clt_id.nextval, 'hans', 'krueger', 4);
insert into lbt_clients values (lbs_clt_id.nextval, 'helmut', 'autobahn', 4);
insert into lbt_clients values (lbs_clt_id.nextval, 'helga', 'sauerkraut', 4);
insert into lbt_clients values (lbs_clt_id.nextval, 'mariusz', 'testowy', 2);