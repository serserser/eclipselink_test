package dom.test.ejb.impl;

import dom.test.cache.DictionaryCache;
import dom.test.domain.CountryEntity;
import dom.test.ejb.CountryValidatorLocal;
import dom.test.ejb.valdation.ValidationUtils;
import dom.test.exceptions.cache.NotFoundException;
import dom.test.exceptions.validation.InvalidDataException;
import dom.test.exceptions.validation.MissingDataException;

import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
public class CountryValidatorBean implements CountryValidatorLocal {

	@EJB
	DictionaryCache dictionaryCache;

	@Override
	public void validateCountry(CountryEntity country) throws MissingDataException, InvalidDataException {
		String prefix = "country";
		ValidationUtils.validateMandatory(prefix, "", country);
		String countryCode = country.getKey();
		ValidationUtils.validateNotEmpty(prefix, "key", countryCode);


	}

	@Override
	public void validateCountryExists(String countryCode) throws InvalidDataException {
		try {
			dictionaryCache.getCountry(countryCode);
		} catch ( NotFoundException exc ) {
			throw new InvalidDataException("No country was found for code: " + countryCode);
		}
	}


}
