package dom.test.ejb;

import dom.test.domain.ClientEntityBean;
import dom.test.exceptions.validation.InvalidDataException;
import dom.test.exceptions.validation.MissingDataException;

import javax.ejb.Local;

@Local
public interface ClientValidatorLocal {

	void validateClient(ClientEntityBean client) throws MissingDataException, InvalidDataException;
}
