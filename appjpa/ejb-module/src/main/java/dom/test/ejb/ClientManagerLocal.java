package dom.test.ejb;

import dom.test.domain.ClientEntityBean;
import dom.test.exceptions.validation.InvalidDataException;
import dom.test.exceptions.validation.MissingDataException;

import javax.ejb.Local;

@Local
public interface ClientManagerLocal {

	void createClient(ClientEntityBean client) throws MissingDataException, InvalidDataException;
}
