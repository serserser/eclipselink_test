package dom.test.ejb.impl;

import dom.test.cache.DictionaryCache;
import dom.test.dao.ClientDAO;
import dom.test.domain.ClientEntityBean;
import dom.test.domain.CountryEntity;
import dom.test.ejb.ClientManagerLocal;
import dom.test.exceptions.validation.InvalidDataException;
import dom.test.exceptions.validation.MissingDataException;
import dom.test.exceptions.cache.NotFoundException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.*;
import javax.inject.Inject;
import java.util.Optional;

@Stateless
public class ClientManagerBean implements ClientManagerLocal {

	private static final Logger logger = LoggerFactory.getLogger(ClientManagerBean.class);

	@EJB
	DictionaryCache dictionaryCache;

	@Inject
	ClientDAO clientDAO;

	@Override
	public void createClient(ClientEntityBean client) throws MissingDataException, InvalidDataException {
		CountryEntity country = client.getCountry();
		if ( country == null || StringUtils.isBlank(country.getKey()) ) {
			throw new MissingDataException("Client must have a country assigned");
		}

		try {
			String countryCode = country.getKey();
			Optional<CountryEntity> optionalCountry = dictionaryCache.getCountry(countryCode);
			CountryEntity countryFromDB = optionalCountry.orElseThrow(() -> new NotFoundException(countryCode));
			client.setCountry(countryFromDB);
		} catch ( NotFoundException exc ) {
			String message = "The country code specified for clients country doens't exist: " + exc.getMessage();
			logger.warn(message);
			throw new InvalidDataException(message);
		}

		clientDAO.createClient(client);
	}
}
