package dom.test.ejb.valdation;

import dom.test.exceptions.validation.MissingDataException;
import org.apache.commons.lang3.StringUtils;

public class ValidationUtils {
	public static void validateMandatory(String prefix, String name, Object object) throws MissingDataException {
		if ( object == null ) {
			throw new MissingDataException(prefix + "." + name + " is mandatory");
		}
	}

	public static void validateNotEmpty(String prefix, String name, String value) throws MissingDataException {
		if ( StringUtils.isEmpty(value) ) {
			throw new MissingDataException(prefix + "." + name + " is mandatory");
		}
	}
}
