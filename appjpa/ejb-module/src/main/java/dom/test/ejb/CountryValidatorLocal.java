package dom.test.ejb;

import dom.test.domain.CountryEntity;
import dom.test.exceptions.validation.InvalidDataException;
import dom.test.exceptions.validation.MissingDataException;

import javax.ejb.Local;

@Local
public interface CountryValidatorLocal {

	void validateCountry(CountryEntity country) throws MissingDataException, InvalidDataException;

	void validateCountryExists(String countryCode) throws InvalidDataException;
}
