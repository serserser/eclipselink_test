package dom.test.ejb.impl;

import dom.test.domain.ClientEntityBean;
import dom.test.domain.CountryEntity;
import dom.test.ejb.ClientValidatorLocal;
import dom.test.ejb.valdation.ValidationUtils;
import dom.test.exceptions.validation.InvalidDataException;
import dom.test.exceptions.validation.MissingDataException;

import javax.ejb.Stateless;

@Stateless
public class ClientValidatorBean implements ClientValidatorLocal {

	@Override
	public void validateClient(ClientEntityBean client) throws MissingDataException, InvalidDataException {
		String prefix = "client";
		ValidationUtils.validateMandatory(prefix, "", client);

		CountryEntity country = client.getCountry();

	}
}
