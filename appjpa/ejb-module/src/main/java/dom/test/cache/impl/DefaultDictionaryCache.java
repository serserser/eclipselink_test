package dom.test.cache.impl;

import dom.test.cache.DictionaryCache;
import dom.test.dao.CountryDAO;
import dom.test.domain.CountryEntity;
import dom.test.exceptions.cache.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.*;
import javax.inject.Inject;
import java.util.List;
import java.util.Optional;


@Singleton
@Startup
public class DefaultDictionaryCache implements DictionaryCache {

	private static final Logger logger = LoggerFactory.getLogger(DefaultDictionaryCache.class);

	@Inject
	CountryDAO countryDAO;

	@PostConstruct
	void initialize() {
		logger.info(DefaultDictionaryCache.class.getName() + " bean created");
	}

	@Override
	@Lock(LockType.READ)
	public List<CountryEntity> getCountries() {
		logger.trace("getCountries called");
		return countryDAO.getAllItems();
	}

	@Override
	@Lock(LockType.READ)
	public Optional<CountryEntity> getCountry(Long id) {
		return countryDAO.getItem(id);
	}

	@Override
	@Lock(LockType.READ)
	public Optional<CountryEntity> getCountry(String code) throws NotFoundException {
		return countryDAO.getItem(code);
	}

	@Override
	public void createCountry(CountryEntity country) {
		countryDAO.save(country);
	}

	@PreDestroy
	void cleanup() {
		logger.info(DefaultDictionaryCache.class.getName() + " bean destroyed");
	}
}
