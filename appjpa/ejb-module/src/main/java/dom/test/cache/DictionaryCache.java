package dom.test.cache;

import dom.test.domain.CountryEntity;
import dom.test.exceptions.cache.NotFoundException;

import javax.ejb.Local;
import java.util.List;
import java.util.Optional;

@Local
public interface DictionaryCache {
	List<CountryEntity> getCountries();

	Optional<CountryEntity> getCountry(Long id);

	Optional<CountryEntity> getCountry(String code) throws NotFoundException;

	void createCountry(CountryEntity country);
}
