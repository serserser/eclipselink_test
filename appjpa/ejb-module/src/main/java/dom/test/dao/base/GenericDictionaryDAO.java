package dom.test.dao.base;


import dom.test.domain.base.DictionaryEntity;

public abstract class GenericDictionaryDAO<T extends DictionaryEntity> extends GenericCodeDao<T> {
	public GenericDictionaryDAO(Class<T> entityClass) {
		super(entityClass);
	}
}
