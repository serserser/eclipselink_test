package dom.test.dao;


import dom.test.constants.JPAConstants;
import dom.test.dao.base.GenericDictionaryDAO;
import dom.test.domain.CountryEntity;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@ApplicationScoped
public class CountryDAO extends GenericDictionaryDAO<CountryEntity> {

	@PersistenceContext(name = JPAConstants.NAUKA_PERSISTENCE_CONTEXT_NAME)
	private EntityManager entityManager;

	public CountryDAO() {
		super(CountryEntity.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}

	CountryDAO(EntityManager em) {
		super(CountryEntity.class);
		entityManager = em;
	}
}
