//package dom.test.dao.impl;
//
//import dom.test.constants.JPAConstants;
//import dom.test.dao.CountryDAO;
//import dom.test.exceptions.JPAQueryException;
//import dom.test.domain.CountryEntityBean;
//import dom.test.exceptions.cache.DuplicatedEntityException;
//import dom.test.exceptions.cache.NotFoundException;
//
//import javax.enterprise.context.ApplicationScoped;
//import javax.persistence.EntityManager;
//import javax.persistence.PersistenceContext;
//import java.util.List;
//
//@ApplicationScoped
//public class CountryDAOImpl extends GenericDAO<CountryEntityBean> implements CountryDAO {
//
//	@PersistenceContext(name = JPAConstants.NAUKA_PERSISTENCE_CONTEXT_NAME)
//	private EntityManager entityManager;
//
//	@Override
//	public List<CountryEntityBean> getCountries() {
//		return CountryEntityBean.getCountries(entityManager);
//	}
//
//	@Override
//	public CountryEntityBean getCountry(Long id) {
//		return CountryEntityBean.getCountryById(entityManager, id);
//	}
//
//	@Override
//	public CountryEntityBean getCountry(String code) throws NotFoundException {
//		return CountryEntityBean.getCountryByCode(entityManager, code);
//	}
//
//	@Override
//	public void createCountry(CountryEntityBean country) {
//		entityManager.persist(country);
//	}
//
//	@Override
//	EntityManager getEntityManager() {
//		return entityManager;
//	}
//}
