package dom.test.dao.base;


import dom.test.domain.base.KeyableEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class GenericKeyDAO<C, T extends KeyableEntity<C>> extends GenericDAO<T> {

	private static final Logger logger = LoggerFactory.getLogger(GenericKeyDAO.class);

	Class<C> entityCodes;

	public GenericKeyDAO(Class<T> entityClass) {
		super(entityClass);
	}

	public Set<C> getAvailableKeys() {
		List<T> allItems = getAllItems();
		return allItems.stream()
				.map(KeyableEntity::getKey)
				.collect(Collectors.toSet());
	}

	public Map<C, T> getAllItemsByCode() {
		List<T> allItems = getAllItems();
		return allItems.stream()
				.collect(Collectors.toMap(KeyableEntity::getKey, item -> item));
	}

	public Optional<T> getItem(String code) {
		EntityManager em = getEntityManager();
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<T> query = criteriaBuilder.createQuery(entityClass);

		Root<T> root = query.from(entityClass);
		query.select(root);
		query.where(criteriaBuilder.equal(root.get("key"), code));

		try {
			return Optional.ofNullable(em.createQuery(query).getSingleResult());
		} catch ( NoResultException exc ) {
			logger.warn("Item with code " + code + " was not found");
			return Optional.empty();
		}
	}
}
