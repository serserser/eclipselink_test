package dom.test.dao.impl;

import dom.test.constants.JPAConstants;
import dom.test.dao.ClientDAO;
import dom.test.domain.ClientEntityBean;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@ApplicationScoped
public class ClientDAOImpl implements ClientDAO {

	@PersistenceContext(name = JPAConstants.NAUKA_PERSISTENCE_CONTEXT_NAME)
	private EntityManager entityManager;

	@Override
	public List<ClientEntityBean> getClients() {
		return ClientEntityBean.getClients(entityManager);
	}

	@Override
	public ClientEntityBean getClient(Long id) {
		return ClientEntityBean.findClient(entityManager, id);
	}

	@Override
	public List<ClientEntityBean> getClientsByCountry(String countryCode) {
		return ClientEntityBean.getClientsByCountry(entityManager, countryCode);
	}

	@Override
	public void createClient(ClientEntityBean client) {
		ClientEntityBean.createClient(entityManager, client);
	}
}
