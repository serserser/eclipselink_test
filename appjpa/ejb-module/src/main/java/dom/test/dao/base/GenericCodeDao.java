package dom.test.dao.base;


import dom.test.domain.base.CodableEntity;

public abstract class GenericCodeDao<T extends CodableEntity> extends GenericKeyDAO<String, T> {
	public GenericCodeDao(Class<T> entityClass) {
		super(entityClass);
	}
}
