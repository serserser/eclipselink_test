package dom.test.dao;

import dom.test.domain.ClientEntityBean;

import java.util.List;

public interface ClientDAO {
	List<ClientEntityBean> getClients();

	ClientEntityBean getClient(Long id);

	void createClient(ClientEntityBean client);

	List<ClientEntityBean> getClientsByCountry(String countryCode);
}
