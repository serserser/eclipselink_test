package dom.test.dao.base;


import dom.test.domain.base.Persistable;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

public abstract class GenericDAO<T extends Persistable> {

	protected Class<T> entityClass;

	public GenericDAO(Class<T> entityClass) {
		this.entityClass = entityClass;
	}

	protected abstract EntityManager getEntityManager();


	public Optional<T> getItem(Long id) {
		return Optional.ofNullable(getEntityManager().find(entityClass, id));
	}

	public List<T> getAllItems() {
		EntityManager em = getEntityManager();
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(entityClass);
		Root<T> rootEntry = criteriaQuery.from(entityClass);
		CriteriaQuery<T> selectAll = criteriaQuery.select(rootEntry);
		TypedQuery<T> query = em.createQuery(selectAll);
		return query.getResultList();
	}

	public boolean contains(Long id) {
		Optional<T> item = getItem(id);
		return item.isPresent();
	}

	public void save(T item) {
		getEntityManager().persist(item);
	}
}
