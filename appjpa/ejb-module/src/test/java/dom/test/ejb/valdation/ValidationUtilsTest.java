package dom.test.ejb.valdation;


import dom.test.exceptions.validation.MissingDataException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ValidationUtilsTest {

	@Test
	void testValidateMandatoryNullFailes() {
		assertThrows(MissingDataException.class, () ->
			ValidationUtils.validateMandatory("", "", null));
	}

	@Test
	void testValidateMandatoryNotNullPasses() throws Exception {
		ValidationUtils.validateMandatory("", "", new String());
	}

	@Test
	void testValidateNotEmptyEmptyFailsOnEmptyString() {
		assertThrows(MissingDataException.class, () ->
			ValidationUtils.validateNotEmpty("", "", null));
		assertThrows(MissingDataException.class, () ->
				ValidationUtils.validateNotEmpty("", "", ""));
		assertThrows(MissingDataException.class, () ->
			ValidationUtils.validateNotEmpty("", "", new String()));
	}

	@Test
	void testValidateNotEmptyPassesOnNotEmptyString() throws Exception {
		ValidationUtils.validateNotEmpty("", "", "janusz");
		ValidationUtils.validateNotEmpty("", "", " ");
		ValidationUtils.validateNotEmpty("", "", "\t");
		ValidationUtils.validateNotEmpty("", "", "\n");
	}
}