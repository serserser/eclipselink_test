package dom.test.ejb.impl;

import dom.test.cache.impl.DefaultDictionaryCache;
import dom.test.dao.impl.ClientDAOImpl;
import dom.test.domain.ClientEntityBean;
import dom.test.domain.CountryEntity;
import dom.test.exceptions.validation.InvalidDataException;
import dom.test.exceptions.validation.MissingDataException;
import dom.test.exceptions.cache.NotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.*;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@RunWith(JUnitPlatform.class)
class ClientManagerBeanTest {

	@Mock
	DefaultDictionaryCache dictionaryCache;

	@Mock
	ClientDAOImpl clientDAO;

	@InjectMocks
	ClientManagerBean clientManagerBean;

	CountryEntity countryEntityBean;
	String existingCode = "EXISTING_CODE";
	String nonExistingCode = "NON_EXISTING_CODE";

	@BeforeEach
	void init() throws NotFoundException {
		countryEntityBean = new CountryEntity();
		countryEntityBean.setKey(existingCode);

		MockitoAnnotations.initMocks(this);

		when(dictionaryCache.getCountry(existingCode)).thenReturn(Optional.of(countryEntityBean));
		when(dictionaryCache.getCountry(nonExistingCode)).thenThrow(NotFoundException.class);
	}

	@Test
	void testClientCreated() throws MissingDataException, InvalidDataException {
		ClientEntityBean clientEntityBean = new ClientEntityBean();
		clientEntityBean.setCountryCode(existingCode);

		clientManagerBean.createClient(clientEntityBean);

		verify(clientDAO, times(1)).createClient(clientEntityBean);
		verify(clientDAO).createClient(ArgumentMatchers.eq(clientEntityBean));
	}

	@Test
	void testExceptionThrownOnNonexistingCountry() {
		ClientEntityBean clientEntityBean = new ClientEntityBean();
		clientEntityBean.setCountryCode(nonExistingCode);

		InvalidDataException exception = expectThrows(InvalidDataException.class, () ->
			clientManagerBean.createClient(clientEntityBean)
		);
	}

	@Test
	void testExceptionThrownOnMissingCountryCode() {
		ClientEntityBean clientEntityBean = new ClientEntityBean();

		MissingDataException exception = expectThrows(MissingDataException.class, () ->
			clientManagerBean.createClient(clientEntityBean)
		);

		assertEquals("Client must have a country assigned", exception.getMessage());
	}
}