package dom.test.dao;

import dom.test.dao.impl.base.DAOIntegrationTestBaseClass;
import dom.test.domain.CountryEntity;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class CountryDAOIntegrationTest extends DAOIntegrationTestBaseClass {

	private static CountryDAO countryDAO;

	@BeforeAll
	static void createDAO() {
		countryDAO = new CountryDAO(entityManager);
	}

	@Test
	void testGettingValuesByCode() {
		Map<String, CountryEntity> allItemsByCode = countryDAO.getAllItemsByCode();
		assertNotNull(allItemsByCode);
		assertEquals(4, allItemsByCode.size());
		assertTrue(allItemsByCode.containsKey("PL"));
		assertEquals("Poland", allItemsByCode.get("PL").getName());
		assertTrue(allItemsByCode.containsKey("DE"));
		assertEquals("Germany", allItemsByCode.get("DE").getName());
		assertTrue(allItemsByCode.containsKey("RU"));
		assertEquals("Russia", allItemsByCode.get("RU").getName());
		assertTrue(allItemsByCode.containsKey("FR"));
		assertEquals("France", allItemsByCode.get("FR").getName());
	}

	@Override
	protected String getDataSetFileName() {
		return "dbData/countryDAOIntegrationTest.xml";
	}
}