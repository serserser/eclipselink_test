package dom.test.dao.base.entities;

import dom.test.domain.base.KeyableEntity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
@AttributeOverride(name = "key", column = @Column(name = "code", unique = true))
public class DummyKeyable extends KeyableEntity<String> {
}
