package dom.test.dao.base;

import dom.test.dao.base.entities.DummyPersistable;
import dom.test.dao.impl.base.DAOIntegrationTestBaseClass;
import dom.test.domain.base.Persistable;
import dom.test.exceptions.validation.InvalidDataException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.persistence.EntityManager;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class GenericDAOIntegrationTest extends DAOIntegrationTestBaseClass {

	private static GenericDAO<DummyPersistable> genericDAO;

	@BeforeAll
	static void initialize() {
		genericDAO = new GenericDAO<DummyPersistable>(DummyPersistable.class) {
			@Override
			protected EntityManager getEntityManager() {
				return entityManager;
			}
		};
	}

	@Test
	void testGetAllItems() {
		List<DummyPersistable> items = genericDAO.getAllItems();
		assertNotNull(items);
		assertEquals(2, items.size());

		assertTrue(items.stream()
				.map(Persistable::getId)
				.filter(aLong -> aLong == 13)
				.count() == 1);
		assertTrue(items.stream()
				.map(Persistable::getId)
				.filter(aLong -> aLong == 14)
				.count() == 1);
	}

	@Test
	@SuppressWarnings("OptionalGetWithoutIsPresent")
	void testGetExistingItem() {
		Optional<DummyPersistable> item = genericDAO.getItem(13L);
		assertNotNull(item);
		assertTrue(item.isPresent());
		DummyPersistable result = item.get();
		assertEquals(Long.valueOf(13), result.getId());
	}

	@Test
	void testGetNonExistingItem() {
		Optional<DummyPersistable> item = genericDAO.getItem(1234L);
		assertNotNull(item);
		assertFalse(item.isPresent());
	}

	@Test
	void testContains() {
		assertTrue(genericDAO.contains(13L));
		assertFalse(genericDAO.contains(123L));
	}

	@Test
	void testSave() throws InvalidDataException {
		EntityManager entityManagerMock = Mockito.mock(EntityManager.class);
		doNothing().when(entityManagerMock).persist(any());
		GenericDAO<DummyPersistable> genericDAOMock = new GenericDAO<DummyPersistable>(DummyPersistable.class) {
			@Override
			protected EntityManager getEntityManager() {
				return entityManagerMock;
			}
		};

		DummyPersistable flagged = new DummyPersistable();
		genericDAOMock.save(flagged);
		verify(entityManagerMock, times(1)).persist(flagged);
	}

	@Override
	protected String getDataSetFileName() {
		return "dbData/genericDAOIntegrationTest.xml";
	}
}