package dom.test.dao.impl.base;

import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.File;
import java.net.URL;

public abstract class DAOIntegrationTestBaseClass {



	private static final String INTEGRATION_PU_NAME = "integration";

	protected static EntityManager entityManager;
	private static EntityManagerFactory entityManagerFactory;
	private static final String DB_DRIVER_CLASS_NAME = "org.apache.derby.jdbc.EmbeddedDriver";

	@BeforeAll
	static void setupDatabase() {
		entityManagerFactory = Persistence.createEntityManagerFactory(INTEGRATION_PU_NAME);
		entityManager = entityManagerFactory.createEntityManager();
	}

	@BeforeEach
	void initializeDB() throws Exception {
		IDatabaseTester tester = new JdbcDatabaseTester(DB_DRIVER_CLASS_NAME,
				"jdbc:derby:memory:testDB;create=true");
		tester.setSetUpOperation(DatabaseOperation.CLEAN_INSERT);
		tester.setDataSet(getDataSet());
		tester.onSetup();
	}

	private IDataSet getDataSet() throws Exception {
		URL dataSetsURL = getClass().getClassLoader().getResource(getDataSetFileName());
		File xmlFile = new File(dataSetsURL.toURI());
		return new FlatXmlDataSetBuilder().build(xmlFile);
	}

	protected abstract String getDataSetFileName();
}
