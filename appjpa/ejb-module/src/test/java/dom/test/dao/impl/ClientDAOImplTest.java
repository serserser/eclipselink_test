package dom.test.dao.impl;

import dom.test.dao.CountryDAO;
import dom.test.domain.CountryEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.persistence.EntityManager;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@RunWith(JUnitPlatform.class)
class ClientDAOImplTest {

	@Mock
	EntityManager entityManager;

	@InjectMocks
	CountryDAO countryDAO;

	Long countryId;

	CountryEntity countryEntityBean;


	@BeforeEach
	void initialize() {
		MockitoAnnotations.initMocks(this);

		countryId = 1L;

		countryEntityBean = new CountryEntity();
		countryEntityBean.setId(countryId);
		countryEntityBean.setName("testCountry");
		countryEntityBean.setKey("TC");

		when(entityManager.find(CountryEntity.class, countryId)).thenReturn(countryEntityBean);
	}
}