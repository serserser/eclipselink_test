package dom.test.dao.base;

import dom.test.dao.base.entities.DummyKeyable;
import dom.test.dao.impl.base.DAOIntegrationTestBaseClass;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class GenericKeyDAOIntegrationTest extends DAOIntegrationTestBaseClass {

	private static GenericKeyDAO<String, DummyKeyable> dao;
	private static final short EXPECTED_VALUES_COUNT = 4;

	@BeforeAll
	static void setupDao() {
		dao = new GenericKeyDAO<String, DummyKeyable>(DummyKeyable.class) {
			@Override
			protected EntityManager getEntityManager() {
				return entityManager;
			}
		};
	}

	@Test
	@SuppressWarnings("OptionalGetWithoutIsPresent")
	void testSelectingByCode() {
		Optional<DummyKeyable> item = dao.getItem("KEY3");
		assertNotNull(item);
		assertTrue(item.isPresent());
		DummyKeyable result = item.get();
		assertEquals("KEY3", result.getKey());
	}
	@Test
	void testGettingAllKeys() {
		Set<String> keys = dao.getAvailableKeys();
		assertNotNull(keys);
		assertEquals(EXPECTED_VALUES_COUNT, keys.size());
		assertTrue(keys.contains("KEY1"));
		assertTrue(keys.contains("KEY2"));
		assertTrue(keys.contains("KEY3"));
		assertTrue(keys.contains("KEY4"));
	}

	@Test
	void testGettingAllItemsByCode() {
		Map<String, DummyKeyable> allItemsByCode = dao.getAllItemsByCode();
		assertNotNull(allItemsByCode);
		assertEquals(4, allItemsByCode.size());
		assertTrue(allItemsByCode.containsKey("KEY1"));
		assertEquals("KEY1", allItemsByCode.get("KEY1").getKey());
		assertTrue(allItemsByCode.containsKey("KEY2"));
		assertEquals("KEY2", allItemsByCode.get("KEY2").getKey());
		assertTrue(allItemsByCode.containsKey("KEY3"));
		assertEquals("KEY3", allItemsByCode.get("KEY3").getKey());
		assertTrue(allItemsByCode.containsKey("KEY4"));
		assertEquals("KEY4", allItemsByCode.get("KEY4").getKey());
	}

	@Override
	protected String getDataSetFileName() {
		return "dbData/genericKeyDAOIntegrationTest.xml";
	}
}