package dom.test.exceptions;

public class JPAQueryException extends RuntimeException {
	public JPAQueryException(String message) {
		super(message);
	}
}
