package dom.test.exceptions.validation;

public class InvalidDataException extends Exception {
	public InvalidDataException(String message) {
		super(message);
	}
}
