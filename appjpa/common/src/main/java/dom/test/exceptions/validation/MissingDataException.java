package dom.test.exceptions.validation;

public class MissingDataException  extends Exception {
	public MissingDataException(String message) {
		super(message);
	}
}
