package dom.test.exceptions.cache;

public class DuplicatedEntityException extends RuntimeException {
	public DuplicatedEntityException(String message) {
		super(message);
	}
}
