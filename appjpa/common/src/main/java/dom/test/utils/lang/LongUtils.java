package dom.test.utils.lang;

public class LongUtils {

	public static boolean areEquals(Long first, Long second) {
		if ( first == null && second == null ) {
			return false;
		} else if ( first != null && ! first.equals(second)
				|| ! second.equals(first) ) {
			return false;
		}
		return true;
	}
}
