package dom.test.utils.lang;

public class CommonUtils {

	public static void validateNotNull(Object objectOne, Object objectTwo) {
		if ( objectOne == null || objectTwo == null ) {
			throw new IllegalArgumentException("Both provided countries have to be not null");
		}
	}

}
