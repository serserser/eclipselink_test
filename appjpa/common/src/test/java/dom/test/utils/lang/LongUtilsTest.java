package dom.test.utils.lang;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(JUnitPlatform.class)
class LongUtilsTest {

	public static final String LONGS_WITH_THE_SAME_VALUE_SHOULD_BE_EQUAL = "Longs with the same value should be equal";
	private static final String LONGS_WITH_DIFFERENT_VALUE_SHOULDNT_BE_EQUAL = "Long with different value shouldn't be equal";

	@Test
	@DisplayName("Check that two equal longs created with autoboxing are equal")
	void testAutoboxingEqual() {
		Long first = 6L;
		Long second = 6L;

		boolean result = LongUtils.areEquals(first, second);

		assertTrue(result, LONGS_WITH_THE_SAME_VALUE_SHOULD_BE_EQUAL);
	}

	@Test
	@DisplayName("Check that two longs created explicitly are equal")
	void testExplicitEqual() {
		Long first = new Long(6);
		Long second = new Long(6);

		boolean result = LongUtils.areEquals(first, second);

		assertTrue(result, LONGS_WITH_THE_SAME_VALUE_SHOULD_BE_EQUAL);
	}

	@Test
	@DisplayName("Check that two equal longs created with autoboxing are equal")
	void testAutoboxingNotEqual() {
		Long first = 6L;
		Long second = - 6L;

		boolean result = LongUtils.areEquals(first, second);

		assertFalse(result, LONGS_WITH_DIFFERENT_VALUE_SHOULDNT_BE_EQUAL);
	}

	@Test
	@DisplayName("Check that two longs created explicitly are equal")
	void testExplicitNotEqual() {
		Long first = new Long(6);
		Long second = new Long(- 6);

		boolean result = LongUtils.areEquals(first, second);

		assertFalse(result, LONGS_WITH_DIFFERENT_VALUE_SHOULDNT_BE_EQUAL);
	}

	@Test
	@DisplayName("Check that null and some value are not equal")
	void testNullAndNotNullNotEqual() {
		Long notNullValue = 10L;
		Long nullValue = null;

		// first value null
		boolean result = LongUtils.areEquals(nullValue, notNullValue);

		assertFalse(result, "null and not null shouldn't be equal");

		result = LongUtils.areEquals(notNullValue, nullValue);

		assertFalse(result, "null and not null shouldn't be equal");
	}

	@Test
	@DisplayName("Test that two nulls are not equal")
	void testNullAndNullNotEqual() {
		Long firstNull = null;
		Long secondNull = null;

		boolean result = LongUtils.areEquals(firstNull, secondNull);

		assertFalse(result, "Two nulls shouldn't be equal");
	}

	@Test
	@DisplayName("Check that value from boxed int is equal")
	void testBoxedIntEqual() {
		int intValue = 9;
		Long longValue = 9L;

		boolean result = LongUtils.areEquals(Integer.toUnsignedLong(intValue), longValue);

		assertTrue(result, LONGS_WITH_THE_SAME_VALUE_SHOULD_BE_EQUAL);
	}
}