package dom.test.utils.lang;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(JUnitPlatform.class)
class CommonUtilsTest {

	@Test
	@DisplayName("Test two null object cause exception to be thrown")
	void testTwoNulls() {
		Long nullLong = null;
		Integer nullInt = null;

		assertThrows(IllegalArgumentException.class, () -> {
			CommonUtils.validateNotNull(nullInt, nullLong);
		});
	}

	@Test
	@DisplayName("Test one null object cause exception to be thrown")
	void testOneNull() {
		String str = null;
		Double dbl = 19.897;

		assertThrows(IllegalArgumentException.class, () -> {
			CommonUtils.validateNotNull(str, dbl);
		});

		assertThrows(IllegalArgumentException.class, () -> {
			CommonUtils.validateNotNull(dbl, str);
		});
	}

	@Test
	@DisplayName("Test two not nulls don't cause exception to be thrown")
	void testNoNulls() {
		String str = "janusz";
		Double dbl = 89.987;

		CommonUtils.validateNotNull(str, dbl);
		CommonUtils.validateNotNull(dbl, str);
	}

}