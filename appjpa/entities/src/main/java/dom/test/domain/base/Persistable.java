package dom.test.domain.base;

import javax.persistence.*;

@MappedSuperclass
public abstract class Persistable {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ID_SEQ_GENERATOR")
	protected Long id;

	public Persistable() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
