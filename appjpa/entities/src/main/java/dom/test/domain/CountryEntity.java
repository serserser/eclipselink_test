package dom.test.domain;

import dom.test.domain.base.DictionaryEntity;

import javax.persistence.*;

@Entity
@Table(name = "LBT_COUNTRIES")
@AttributeOverrides({
		@AttributeOverride(name = "id", column = @Column(name = "CTR_ID")),
		@AttributeOverride(name = "key", column = @Column(name = "CTR_CODE", unique = true, nullable = false, length = 2)),
		@AttributeOverride(name = "name", column = @Column(name = "CTR_NAME", length = 100))
})
@SequenceGenerator(
		name = "ID_SEQ_GENERATOR",
		sequenceName = "LBS_CTR_ID",
		allocationSize = 1
)
public class CountryEntity extends DictionaryEntity {

	public CountryEntity() {
	}

	public CountryEntity(String key, String name) {
		super(key, name);
	}
}