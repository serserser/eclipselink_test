package dom.test.domain;

import javax.persistence.*;
import java.util.List;

@Entity(name = "ClientEntityBean")
@Table(name = "LBT_CLIENTS")
@SequenceGenerator(
		name = "LBS_CLT_ID",
		sequenceName = "LBS_CLT_ID",
		allocationSize = 1
)
@NamedQueries({
		@NamedQuery(name = ClientEntityBean.GET_CLIENTS, query = "select client from ClientEntityBean client"),
		@NamedQuery(name = ClientEntityBean.GET_CLIENTS_BY_COUNTRY, query = "select client from ClientEntityBean client where client.country.key = :countryCode")
})
@Access(AccessType.FIELD)
public class ClientEntityBean {

	protected static final String GET_CLIENTS = "getClients";
	protected static final String GET_CLIENTS_BY_COUNTRY = "getClientsByCountry";

	@Id
	@Column(name = "CLT_ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LBS_CLT_ID")
	Long id;

	@Column(name = "CLT_FIRST_NAME")
	String firstName;

	@Column(name = "CLT_LAST_NAME")
	String lastName;

	@ManyToOne
	@JoinColumn(name = "CLT_CTR_ID")
	CountryEntity country = new CountryEntity();

	@SuppressWarnings("unchecked")
	public static List<ClientEntityBean> getClients(EntityManager entityManager) {
		return entityManager.createNamedQuery(GET_CLIENTS)
				.getResultList();
	}

	public static ClientEntityBean findClient(EntityManager entityManager, Long id) {
		return entityManager.find(ClientEntityBean.class, id);
	}

	@SuppressWarnings("unchecked")
	public static List<ClientEntityBean> getClientsByCountry(EntityManager entityManager, String countryCode) {
		return entityManager.createNamedQuery(GET_CLIENTS_BY_COUNTRY)
				.setParameter("countryCode", countryCode)
				.getResultList();
	}


	public static void createClient(EntityManager entityManager, ClientEntityBean clientEntityBean) {
		entityManager.persist(clientEntityBean);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public CountryEntity getCountry() {
		return country;
	}

	public void setCountry(CountryEntity country) {
		this.country = country;
	}

	public void setCountryCode(String countryCode) {
		this.getCountry().setKey(countryCode);
	}
}
