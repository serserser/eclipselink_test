package dom.test.domain.base;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class DictionaryEntity extends CodableEntity {

	String name;

	public DictionaryEntity() {
	}

	public DictionaryEntity(String key, String name) {
		super(key);
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
