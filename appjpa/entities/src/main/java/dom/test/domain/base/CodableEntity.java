package dom.test.domain.base;

public class CodableEntity extends KeyableEntity<String> {

	public CodableEntity() {
	}

	public CodableEntity(String key) {
		super(key);
	}
}
