package dom.test.domain.base;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class KeyableEntity<K> extends Persistable {

	@Column(name = "KEY", unique = true)
	protected K key;

	public KeyableEntity() {
	}

	public KeyableEntity(K key) {
		this.key = key;
	}

	public K getKey() {
		return key;
	}

	public void setKey(K code) {
		this.key = code;
	}
}
